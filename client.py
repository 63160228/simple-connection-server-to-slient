import socket

def client_program():
    host = '127.0.0.1'
    port = 5000

    client_socket = socket.socket()
    client_socket.connect((host, port))

    num = 1
    while num <= 100:
        client_socket.send(str(num).encode())
        data = client_socket.recv(1024).decode()
        print('Received from server: ' + data)
        num = int(data) + 1

    client_socket.close()

if __name__ == '__main__':
    client_program()